package task1;

import java.util.Objects;
import java.util.Random;

public class Model {
    // Constructor
    private int initValue;
    private int minValue;
    private int maxValue;

    Model() {
        minValue = 0;
        maxValue = 100;
    }

    // Action method
    public byte compareInitWithNum(int num) throws IllegalArgumentException {
        if (checkBounds(num)) {
            if (num == initValue) {
                return 0;
            } else if (num > initValue) {
                setMaxValue(num);
                return 1;
            } else {
                setMinValue(num);
                return -1;
            }
        }
        throw new IllegalArgumentException();
    }

    // Utility method
    private boolean checkBounds(int num) {
        if (num >= minValue && num <= maxValue) {
            return true;
        }
        return false;
    }

    // Setters
    public void setMinValue(int minValue) {
        this.minValue = minValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }

    public void setInitValue(int initValue) throws IllegalArgumentException {
        if (initValue <= maxValue && initValue >= minValue) {
            this.initValue = initValue;
        }
        else throw new IllegalArgumentException();
    }

    // Getters
    public int getMinValue() {
        return minValue;
    }

    public int getMaxValue() {
        return maxValue;
    }
}
