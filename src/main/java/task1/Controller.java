package task1;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;
import java.util.Scanner;

public class Controller {
    ArrayList<Integer> history = new ArrayList<>();

    // Constructor
    private Model model;
    private View view;


    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    // The Work method
    public void processUser(){
        initializeRandNumber();
        Scanner sc = new Scanner(System.in);
        view.printMessage(View.HELLO);

        while (true) {
            try {
                int inNum = inputIntValueWithScanner(sc);
                history.add(inNum);
                byte result = model.compareInitWithNum(inNum);

                if (result == 0) {
                    view.printMessageAndInt(View.CORRECT, inNum);
                    view.printMessage(View.HISTORY + history);
                    break;
                } else if (result == 1) {
                    view.printMessageAndInt(View.MORE, inNum);
                } else {
                    view.printMessageAndInt(View.LESS, inNum);
                }
            } catch (IllegalArgumentException e) {
                view.printMessage(View.WRONG_NUM);
            }
        }
    }

    // Utility
    private void initializeRandNumber() {
        try {
            model.setInitValue( rand(new int[] {2, 1}) );
        } catch (IllegalArgumentException | ArithmeticException e) {
            model.setInitValue(rand());
        }
    }

    private int inputIntValueWithScanner(Scanner sc) {
        view.printRange(model.getMinValue(), model.getMaxValue());
        view.printMessage(View.INPUT_INT);

        while ( ! sc.hasNextInt()) {
            view.printMessage(View.WRONG_NUM);
            view.printMessage(View.INPUT_INT);
            sc.next();
        }
        return sc.nextInt();
    }

    private int rand() {
        return new Random().nextInt(new Random().nextInt(101));
    }

    private int rand(int[] arr) throws ArithmeticException {
        if (!Objects.isNull(arr) && arr.length == 2 && arr[0] <= arr[1]) {
            return new Random().nextInt(arr[1] - arr[0] + 1) + arr[0];
        }
        throw new ArithmeticException();
    }

}
