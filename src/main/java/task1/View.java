package task1;

public class View {
    // Text's constants
    public static final String HELLO = "Hello! Your aim is to guess a number";
    public static final String INPUT_INT = "Your num = ";
    public static final String WRONG_NUM = "Invalid number. Try again";
    public static final String CORRECT = "Congratulations!!! Right number = ";
    public static final String MORE = "Your num is more than our. Max value = ";
    public static final String LESS = "Your num is less than our. Min value = ";
    public static final String RANGE = "Num range: [%d:%d]. ";
    public static final String HISTORY = "History: ";


    public void printMessage(String str) {
        System.out.println(str);
    }

    public void printMessageAndInt(String str, int num) {
        System.out.println(str + num);
    }

    public void printRange(int num1, int num2) {
        System.out.printf(RANGE, num1, num2);
    }

}
